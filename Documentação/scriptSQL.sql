SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;


CREATE TABLE IF NOT EXISTS `mydb`.`Usuario` (
  `idUsuario` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `senha` VARCHAR(45) NOT NULL,
  `Usuariocol` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idUsuario`))
ENGINE = InnoDB;



CREATE TABLE IF NOT EXISTS `mydb`.`Post` (
  `idPost` INT NOT NULL AUTO_INCREMENT,
  `content` VARCHAR(400) NOT NULL,
  `Usuario_idUsuario` INT NOT NULL,
  PRIMARY KEY (`idPost`),
  INDEX `fk_Post_Usuario_idx` (`Usuario_idUsuario` ASC),
  CONSTRAINT `fk_Post_Usuario`
    FOREIGN KEY (`Usuario_idUsuario`)
    REFERENCES `mydb`.`Usuario` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `mydb`.`Usuario_has_curtidasPost` (
  `Usuario_idUsuario` INT NOT NULL,
  `curtidasPost_idcurtidasPost` INT NOT NULL,
  PRIMARY KEY (`Usuario_idUsuario`, `curtidasPost_idcurtidasPost`),
  INDEX `fk_Usuario_has_curtidasPost_Usuario1_idx` (`Usuario_idUsuario` ASC),
  CONSTRAINT `fk_Usuario_has_curtidasPost_Usuario1`
    FOREIGN KEY (`Usuario_idUsuario`)
    REFERENCES `mydb`.`Usuario` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `mydb`.`Usuario_has_Usuario` (
  `Usuario_idUsuario` INT NOT NULL,
  `Usuario_idUsuario1` INT NOT NULL,
  PRIMARY KEY (`Usuario_idUsuario`, `Usuario_idUsuario1`),
  INDEX `fk_Usuario_has_Usuario_Usuario2_idx` (`Usuario_idUsuario1` ASC),
  INDEX `fk_Usuario_has_Usuario_Usuario1_idx` (`Usuario_idUsuario` ASC),
  CONSTRAINT `fk_Usuario_has_Usuario_Usuario1`
    FOREIGN KEY (`Usuario_idUsuario`)
    REFERENCES `mydb`.`Usuario` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Usuario_has_Usuario_Usuario2`
    FOREIGN KEY (`Usuario_idUsuario1`)
    REFERENCES `mydb`.`Usuario` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `mydb`.`CommentPost` (
  `idCommentPost` INT NOT NULL AUTO_INCREMENT,
  `Usuario_idUsuario` INT NOT NULL,
  `content` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`idCommentPost`),
  INDEX `fk_CommentPost_Usuario1_idx` (`Usuario_idUsuario` ASC),
  CONSTRAINT `fk_CommentPost_Usuario1`
    FOREIGN KEY (`Usuario_idUsuario`)
    REFERENCES `mydb`.`Usuario` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `mydb`.`Like_has_Like` (
  `idCommentPost` INT NOT NULL,
  `Usuario_idUsuario` INT NOT NULL,
  `status` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`idCommentPost`, `Usuario_idUsuario`),
  INDEX `fk_Like_has_Like_Usuario1_idx` (`Usuario_idUsuario` ASC),
  CONSTRAINT `fk_Like_has_Like_CommentPost1`
    FOREIGN KEY (`idCommentPost`)
    REFERENCES `mydb`.`CommentPost` (`idCommentPost`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Like_has_Like_Usuario1`
    FOREIGN KEY (`Usuario_idUsuario`)
    REFERENCES `mydb`.`Usuario` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `mydb`.`Post_has_Like` (
  `Post_idPost` INT NOT NULL,
  `status` INT NOT NULL DEFAULT 0,
  `Usuario_idUsuario` INT NOT NULL,
  PRIMARY KEY (`Post_idPost`, `Usuario_idUsuario`),
  INDEX `fk_Post_has_Usuario_Post1_idx` (`Post_idPost` ASC),
  INDEX `fk_Post_has_Like_Usuario1_idx` (`Usuario_idUsuario` ASC),
  CONSTRAINT `fk_Post_has_Usuario_Post1`
    FOREIGN KEY (`Post_idPost`)
    REFERENCES `mydb`.`Post` (`idPost`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Post_has_Like_Usuario1`
    FOREIGN KEY (`Usuario_idUsuario`)
    REFERENCES `mydb`.`Usuario` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `mydb`.`SubComment` (
  `idSubComment` INT NOT NULL AUTO_INCREMENT,
  `CommentPost_idCommentPost` INT NOT NULL,
  `content` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`idSubComment`, `CommentPost_idCommentPost`),
  INDEX `fk_SubComment_CommentPost1_idx` (`CommentPost_idCommentPost` ASC),
  CONSTRAINT `fk_SubComment_CommentPost1`
    FOREIGN KEY (`CommentPost_idCommentPost`)
    REFERENCES `mydb`.`CommentPost` (`idCommentPost`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `mydb`.`SubComment_Like` (
  `SubComment` INT NOT NULL,
  `Usuario_idUsuario` INT NOT NULL,
  `status` INT NOT NULL DEFAULT 0,
  INDEX `fk_SubComment_Like_SubComment1_idx` (`SubComment` ASC),
  PRIMARY KEY (`Usuario_idUsuario`, `SubComment`),
  CONSTRAINT `fk_SubComment_Like_SubComment1`
    FOREIGN KEY (`SubComment`)
    REFERENCES `mydb`.`SubComment` (`idSubComment`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_SubComment_Like_Usuario1`
    FOREIGN KEY (`Usuario_idUsuario`)
    REFERENCES `mydb`.`Usuario` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
