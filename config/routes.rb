Rails.application.routes.draw do

  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'

  get 'sessions/new'
  resources :posts
  resources :users

  resources :posts do 
    member do 
      put 'like' => "posts#like"
      put 'dislike' => "posts#dislike"
    end
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
